# CRUD with Go

This simple project is a CRUD developed with GO 1.20. The project's objetive is to organize tasks with a simple terminal interface.


## Installing

We online need download the project from the repository and run the file "main.go" with the following command:

`go run main.go`

this command will show the user helper.

## Tools

The project has the following options to run the main file:

- list : show the list's tasks
- add : add a task
- complete : check a complete task
- delete : delete a task